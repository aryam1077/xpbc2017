<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login/frontend/Login';
$route['login'] = 'login/frontend/Login';
$route['login/validate_user'] = 'login/backend/ValidateUser/newUser';
//Tabla Usuario
$route['menu_principal'] = 'MainMenu';
$route['usuario-agregarUsuario'] = 'usuario/AgregarUsuario';
$route['usuario-modificarUsuario'] = 'usuario/ModificarUsuario';
$route['usuario-eliminarUsuario'] = 'usuario/EliminarUsuario';
$route['usuario-consultarUsuario'] = 'usuario/ConsultarUsuario';

//Tabla Rol
$route['rol-agregarRol'] = 'rol/AgregarRol';
$route['rol-modificarRol'] = 'rol/ModificarRol';
$route['rol-eliminarRol'] = 'rol/EliminarRol';
$route['rol-consultarRol'] = 'rol/ConsultarRol';

//Tabla Tipo Amonestacion
$route['tipoAmonestacion-agregarTipoAmonestacion'] = 'tipoAmonestacion/AgregarTipoAmonestacion';
$route['tipoAmonestacion-modificarTipoAmonestacion'] = 'tipoAmonestacion/ModificarTipoAmonestacion';
$route['tipoAmonestacion-eliminarTipoAmonestacion'] = 'tipoAmonestacion/EliminarTipoAmonestacion';
$route['tipoAmonestacion-consultarTipoAmonestacion'] = 'tipoAmonestacion/ConsultarTipoAmonestacion';

//Tabla felicitaciones
$route['crear_felicitaciones'] = 'felicitaciones/frontend/CrearFelicitacion';
$route['editar_felicitaciones'] = 'felicitaciones/frontend/EditarFelicitacion';
$route['eliminar_felicitaciones'] = 'felicitaciones/frontend/EliminarFelicitacion';
$route['consultar_felicitaciones'] = 'felicitaciones/frontend/ConsultarFelicitacion';

//tabla amonestaciones
$route['crear_amonestaciones'] = 'amonestaciones/frontend/CrearAmonestacion';
$route['editar_amonestaciones'] = 'amonestaciones/frontend/EditarAmonestacion';
$route['eliminar_amonestaciones'] = 'amonestaciones/frontend/EliminarAmonestacion';
$route['consultar_amonestaciones'] = 'amonestaciones/frontend/ConsultarAmonestacion';


$route['404_override'] = 'Error404';
$route['translate_uri_dashes'] = FALSE;

$route['eliminar_usuario'] = 'usuario/eliminarUsuario';

$route['capacitacion/registrar_capacitacion'] = 'capacitaciones/frontend/RegistrarCapacitacion';
