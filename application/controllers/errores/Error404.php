<?php

  if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Error404 extends CI_Controller {
  
   public function index(){
       if($this->session->userdata('is_logued_in') == TRUE) {

         $data['pantallas'] = $this->session->userdata('pantallas');
         $data['menu'] = array_unique(array_column($data['pantallas'], 'GRUPO_PANTALLA'));
         //Load Main View
         $this->load->view("/headers/head_main_menu");
         $this->load->view("/administrator/navigation");
         $this->load->view('/administrator/menu',$data);
         $this->load->view("/errors/cli/error_404");
         $this->load->view("/footers/footer_main_menu");
       } else {
         redirect (base_url().'login');
       }
   }
}
