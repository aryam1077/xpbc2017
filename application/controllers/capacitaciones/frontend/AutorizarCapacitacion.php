<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @Author: RH07022
 * @version: 1.0
 */
class AutorizarCapacitacion extends CI_Controller {

	/**
	 * Login Controller index
	 */
	 public function index() {
     if($this->session->userdata('is_logued_in') == TRUE) {
       $data['pantallas'] = $this->session->userdata('pantallas');
       $data['menu'] = array_unique(array_column($data['pantallas'], 'GRUPO_PANTALLA'));

       //Load Main View
       $this->load->view("/headers/head_auto_cap");
       $this->load->view("/administrator/navigation");
       $this->load->view('/administrator/menu',$data);
       $this->load->view('/administrator/capacitaciones/autorizar_capacitacion');
       $this->load->view("/footers/footer_auto_cap");

       //echo "Menu: ";
       //echo "Nombre Usuario: ". $this->session->userdata('username');
       //print_r($data);

     } else {
       // Load Login View
       redirect (base_url().'login');
     }
   }

}
