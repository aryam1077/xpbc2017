<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @Author: RH07022
 * @version: 1.0
 */
class Login extends CI_Controller {

	/**
	 * Login Controller index
	 */
	public function index() {
		$data['title'] = 'Login';

		$this->load->view("/headers/head_login", $data);
    $this->load->view("/login/login");
    $this->load->view("/footers/footer_login");
	}

}
