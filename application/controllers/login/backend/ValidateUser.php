<?php

/**
 * @Author: RH07022
 * @version: 1.0
 */
class ValidateUser extends CI_Controller {

  /*
   * Construct
   */
  public function __construct() {
    parent::__construct();
    $this->load->model('Login_model');
    $this->load->model('Menu_model');
  }

  /*
   * Index method
   */
  public function index() {
    if($this->session->is_logued_in == TRUE) {
      // Load main menu view
      redirect(base_url().'menu_principal');
    } else {
      // Load Login view
      redirect(base_url().'login');
    }
  }

  /*
   * Validate user data
   */
  public function newUser() {
    if($this->input->post('hiddenToken') == $this->session->userdata('token')) {
      $username = $this->input->post('username');
      $password = sha1($this->input->post('password'));
      $check_user = $this->Login_model->getUserData($username, $password);

      if($check_user == TRUE) {
        $idRol = $check_user[0]->ID_ROL;
        $pantallas = $this->Menu_model->getMenu($check_user[0]->ID_ROL);

        // Create cookie session
        $data = array(
               'is_logued_in' => TRUE,
               'username' => $username,
               'password' => sha1($this->input->post('password')),
               'pantallas' => $pantallas
        );
      }

      // Add cookie in token session
     $this->session->set_userdata($data);
     $this->index();
    } else {
      // Invalid token
      redirect(base_url().'login');
    }
  }

  /*
   * Kill session
   */
  public function logoutCi() {
    $this->session->sess_destroy();
    $this->index();
  }

}

?>
