<?php

/**
 * @Autor: GG06011
 * @versión: 1.0
 */
class consultarRol extends CI_Controller {

  /*
   * Index method
   */
  public function index() {
    if($this->session->userdata('is_logued_in') == TRUE) {
      $data['pantallas'] = $this->session->userdata('pantallas');
      $data['menu'] = array_unique(array_column($data['pantallas'], 'GRUPO_PANTALLA'));

      //Load Main View
      $this->load->view("/headers/head_main_menu");
      $this->load->view("/administrator/navigation");
      $this->load->view('/administrator/menu',$data);
      $this->load->view("/administrator/rol/consultarRol");
      $this->load->view("/footers/footer_main_menu");

      //echo "Menu: ";
      //echo "Nombre Usuario: ". $this->session->userdata('username');
      //print_r($data);

    } else {
      // Load Login View
      redirect (base_url().'login');
    }
  }

}
