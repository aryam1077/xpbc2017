<?php

/**
 * @Author: RH07022
 * @version: 1.0
 */
class Login_model extends CI_Model {

  /**
   * Construct
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Get user information from usuario table
   */
  public function getUserData($username, $password) {
    $this->db->where('nombre_usuario',$username);
    $this->db->where('contrasenia',$password);
    $query = $this->db->get('usuario');

    if($query->num_rows() == 1) {
      // Find user
      return $query->result();
    } else {
      // User not found
      $this->session->set_flashdata('invalid_user','Credenciales inválidas');
      redirect(base_url().'login','refresh');
    }

  }

}
