<!-- start: PAGE -->
			<div class="main-content">
				<!-- start: PANEL CONFIGURATION MODAL FORM -->
				<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Panel Configuration</h4>
							</div>
							<div class="modal-body">
								Here will be a configuration form
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Close
								</button>
								<button type="button" class="btn btn-primary">
									Save changes
								</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- end: STYLE SELECTOR BOX -->
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
								<li>
									<i class="clip-user-3"></i>
									<a href="#">
										Usuario
									</a>
								</li>
								<li class="active">
									Agregar Usuario
								</li>
							</ol>
							<div class="page-header">
						</div>
							<!-- end: PAGE TITLE & BREADCRUMB -->
						</div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: PAGE CONTENT -->
					<div class="row" >
						<div class="col-md-12">
							<!-- start: FORM VALIDATION 1 PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="clip-data"></i>
									Agregar Usuario
									<div class="panel-tools">
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-close" href="#">
											<i class="fa fa-times"></i>
										</a>
									</div>
								</div>
								<div class="panel-body">
									<center><h4><i class="fa fa-user"></i> REGISTRO DE USUARIOS</h4></center>
									<hr>
									<form action="#" role="form" id="form" class="form-horizontal">
										<div class="row">
											<div class="col-md-12">
												<div class="errorHandler alert alert-danger no-display">
													<i class="fa fa-times-sign"></i> se tienen algunos errores. Por favor verificalos.
												</div>
												<div class="successHandler alert alert-success no-display">
													<i class="fa fa-ok"></i> Your form validation is successful!
												</div>
											</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Codigo de Usuario <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="text" placeholder="" class="form-control" id="idUsuario" name="idUsuario">
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Nombre de Usuario <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="text" placeholder="" class="form-control" id="nombreUsuario" name="nombreUsuario">
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Contraseña <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="password" placeholder="[A-Z,a-z,*_[0-9]" class="form-control" id="contraseña" name="contraseña">
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Confirmar Contraseña <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="password" placeholder="[A-Z,a-z,*_[0-9]" class="form-control" id="contraseña2" name="contraseña2">
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Estado <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
														<select name="estado" id="estado" class="form-control" >
																	<option value="">seleccionar</option>
																	<option value="01">Activo</option>
																	<option value="02">Inactivo</option>
														</select>
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Codigo de Rol <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="text" placeholder="" class="form-control" id="idRol" name="idRol">
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Codigo de Empleado <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="text" placeholder="" class="form-control" id="idEmpleado" name="idEmpleado">
												  </div>
												</div>
										</div>
										<div class="row">
												<center>
												<div class="col-sm-4">
													<span class="symbol required"></span>Campos requeridos
													<hr>
												</div>
											</center>
										</div>
										<div class="row" class="modal-footer" align="right">
										<div class="col-md-12" class="pull-right">
											<button <a class="btn btn-primary" href="#" type="submit">
													Cancelar <i class="glyphicon glyphicon-remove-circle"></i>
											</button>
											<button class="btn btn-primary" href="#" type="submit">
														Guardar <i class="fa fa-floppy-o"></i>
												</button>
										</div>
									</div>
									</form>
								</div>
							</div>
							<!-- end: FORM VALIDATION 1 PANEL -->
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
			</div>
			<!-- end: PAGE -->
