<!-- start: PAGE.AgregartipoAmonestacion -->
			<div class="main-content">
				<!-- start: PANEL CONFIGURATION MODAL FORM -->
				<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Panel Configuration</h4>
							</div>
							<div class="modal-body">
								Here will be a configuration form
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Close
								</button>
								<button type="button" class="btn btn-primary">
									Save changes
								</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- end: STYLE SELECTOR BOX -->
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
								<li>
									<i class=" fa fa-gavel"></i>
									<a href="#">
										Tipo de Amonestacion
									</a>
								</li>
								<li class="active">
									Agregar Tipo de Amonestacion
								</li>
							</ol>
							<div class="page-header">
						</div>
							<!-- end: PAGE TITLE & BREADCRUMB -->
						</div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: PAGE CONTENT -->
					<div class="row" >
						<div class="col-md-12">
							<!-- start: FORM VALIDATION 1 PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="clip-data"></i>
									Agregar Tipo de Amonestacion
									<div class="panel-tools">
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-close" href="#">
											<i class="fa fa-times"></i>
										</a>
									</div>
								</div>
								<div class="panel-body">
									<center><h4><i class=" fa fa-gavel"></i> REGISTRO DE TIPO DE AMONESTACION</h4></center>
									<hr>
									<form action="#" role="form" id="form" class="form-horizontal">
											<div class="col-md-12">
												<div class="errorHandler alert alert-danger no-display">
													<i class="fa fa-times-sign"></i> se tienen algunos errores. Por favor verificalos.
												</div>
												<div class="successHandler alert alert-success no-display">
													<i class="fa fa-ok"></i> Your form validation is successful!
												</div>
											</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Codigo de Tipo Amonestacion<span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="text" placeholder="" class="form-control" id="idTipoAmonestacion" name="idTipoAmonestacion">
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Nombre de Tipo de Amonestacion <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													 <input type="text" placeholder="" class="form-control" id="nombreTipoAmonestacion" name="nombreeTipoAmonestacion">
												  </div>
												</div>
												<div class="form-group">
													<label class="col-sm-4 control-label">
														Descripcion <span class="symbol required"></span>
													</label>
													<div class="col-sm-4">
													  <textarea class="autosize form-control" placeholder="" class="form-control" id="descripciontipoAmonestacion" name="descripciontipoAmonestacion"></textarea>
												  </div>
												</div>
												<div class="row">
														<center><br>
														<div class="col-sm-3">
															<span class="symbol required"></span>Campos requeridos
															<hr>
														</div>
													</center>
												</div>
										<div class="row" class="modal-footer" align="right">
										<div class="col-md-12" class="pull-right">
											<button <a class="btn btn-primary" href="#" type="submit">
													Cancelar <i class="glyphicon glyphicon-remove-circle"></i>
											</button>
											<button class="btn btn-primary" href="#" type="submit">
														Guardar <i class="fa fa-floppy-o"></i>
												</button>
										</div>
									</div>
									</form>
								</div>
							</div>
							<!-- end: FORM VALIDATION 1 PANEL -->
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
			</div>
			<!-- end: PAGE -->
