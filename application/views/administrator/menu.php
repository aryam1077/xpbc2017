		<!-- start: MAIN CONTAINER -->
		<div class="main-container">
			<div class="navbar-content">

				<!-- start: SIDEBAR -->
				<div class="main-navigation navbar-collapse collapse">

					<!-- start: MAIN MENU TOGGLER BUTTON -->
					<div class="navigation-toggler">
						<i class="fa fa-compress"></i>

					</div>
					<!-- end: MAIN MENU TOGGLER BUTTON -->

					<!-- start: MAIN NAVIGATION MENU -->
					<ul class="main-navigation-menu">

						<li>
							<a href="menu_principal"><i class="clip-home-3"></i>
								<span class="title">INICIO</span><span class="selected"></span>
							</a>
						</li>

						<?php foreach ($menu as $item) { ?>
						<li>
							<a href="javascript:void(0)">
									<span class="title"> <?php echo $item; ?>
									</span><i class="icon-arrow"></i>
									<span class="selected"></span>
							</a>
							<ul class="sub-menu">
										<?php foreach ($pantallas as $pantalla) {
													if ($pantalla->GRUPO_PANTALLA == $item) { ?>
													<li>
														<a href="<?php echo $pantalla->URL_PANTALLA; ?>">
															<i class="fa <?php echo $pantalla->ICONO_PANTALLA; ?>"></i>
															<span class="title"> <?php echo $pantalla->NOMBRE_PANTALLA; ?> </span>
														</a>
													</li>
										<?php } }?><!--cierre de foreach $pantalla y if-->
							</ul>
						</li>
						<?php } ?><!--cierre de foreach $menu-->

					</ul>
					<!-- end: MAIN NAVIGATION MENU -->

				</div>
				<!-- end: SIDEBAR -->

			</div>
		</div>
		<!-- end: MAIN CONTAINER -->
