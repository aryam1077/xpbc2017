<!-- start: PAGE -->
			<div class="main-content">
				<!-- start: PANEL CONFIGURATION MODAL FORM -->
				<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Panel Configuration</h4>
							</div>
							<div class="modal-body">
								Here will be a configuration form
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Close
								</button>
								<button type="button" class="btn btn-primary">
									Save changes
								</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
								<li>
									<i class="clip-pencil"></i>
									<a href="#">
										Empleados
									</a>
								</li>
								<li class="active">
									Agregar Empleado
								</li>
							</ol>
							<div class="page-header">
								<h1>Agregar empleado</h1>
							</div>
							<!-- end: PAGE TITLE & BREADCRUMB -->
						</div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: FORM WIZARD PANEL -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-external-link-square"></i>
									Agregar nuevo empleado
									<div class="panel-tools">
										<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
										</a>
										<a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
											<i class="fa fa-wrench"></i>
										</a>
										<a class="btn btn-xs btn-link panel-refresh" href="#">
											<i class="fa fa-refresh"></i>
										</a>
										<a class="btn btn-xs btn-link panel-expand" href="#">
											<i class="fa fa-resize-full"></i>
										</a>
										<a class="btn btn-xs btn-link panel-close" href="#">
											<i class="fa fa-times"></i>
										</a>
									</div>
								</div>
								<div class="panel-body">
									<form action="#" role="form" class="smart-wizard form-horizontal" id="form">
										<div id="wizard" class="swMain">
											<ul>
												<li>
													<a href="#step-1">
														<div class="stepNumber">
															1
														</div>
														<span class="stepDesc"> Paso 1
															<br />
															<small>Datos personales</small> </span>
													</a>
												</li>
												<li>
													<a href="#step-2">
														<div class="stepNumber">
															2
														</div>
														<span class="stepDesc"> Paso 2
															<br />
															<small>Estudios realizados y Experiencia laboral</small> </span>
													</a>
												</li>
												<li>
													<a href="#step-3">
														<div class="stepNumber">
															3
														</div>
														<span class="stepDesc"> Paso 3
															<br />
															<small>Grupo Familiar y Referencias</small> </span>
													</a>
												</li>
												<li>
													<a href="#step-4">
														<div class="stepNumber">
															4
														</div>
														<span class="stepDesc"> Paso 4
															<br />
															<small></small>Confirmación</span>
													</a>
												</li>
											</ul>
											<div class="progress progress-striped active progress-sm">
												<div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
													<span class="sr-only"> 0% Complete (success)</span>
												</div>
											</div>
											<div id="step-1">
												<h2 class="StepTitle">Datos personales</h2>
                        <div class="col-sm-12">
												  <div class="fileupload fileupload-new" data-provides="fileupload">
													  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
													  </div>
													  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
													  <div>
														  <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Selecione una fotografía</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Cambiar fotografía</span>
															  <input type="file">
														  </span>
														  <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
															  <i class="fa fa-times"></i> Remover fotografía
														  </a>
													  </div>
												  </div>
                        </div>
												<div class="col-sm-6">
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
  														  Primer nombre <span class="symbol required"></span>
  													  </label>
                            </div>
  													<div class="col-sm-12">
                              <input type="text" class="form-control" id="id_first_name" name="first_name" placeholder="Primer nombre">
    												</div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Primer apellido <span class="symbol required"></span>
    												  </label>
                            </div>
    												<div class="col-sm-12">
                              <input type="text" class="form-control" id="id_first_last_name" name="first_last_name" placeholder="Primer apellido">
      											</div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Tercer nombre
    												  </label>
                            </div>
    												<div class="col-sm-12">
                              <input type="text" class="form-control" id="id_third_name" name="third_name" placeholder="Tercer nombre">
      											</div>
                          </div>
                          <div class="row">
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  DUI <span class="symbol required"></span>
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_dui" name="dui" placeholder="Número de DUI">
                                </div>
  														</div>
  													</div>
														<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  ISSS
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_isss" name="isss" placeholder="Número de ISSS">
                                </div>
  														</div>
  													</div>
  												</div>
                          <div class="row">
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  Número de licencia
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_licence_number" name="licence_number" placeholder="Número de licencia">
                                </div>
  														</div>
  													</div>
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  Tipo de sangre
  															  </label>
                                </div>
                                <div class="col-md-12">
                                  <select name="dd" id="dd" class="form-control" >
      														  <option value="blood_empty">Seleccionar...</option>
      														  <option value="blood_ab+">AB+</option>
																		<option value="blood_ab-">AB-</option>
																		<option value="blood_a+">A+</option>
																		<option value="blood_a-">A-</option>
																		<option value="blood_b+">B+</option>
																		<option value="blood_b-">B-</option>
																		<option value="blood_o+">O+</option>
																		<option value="blood_o-">O-</option>
      														</select>
                                </div>
  														</div>
  													</div>
  												</div>
                          <div class="row">
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  Teléfono
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_phone" name="phone" placeholder="Número de teléfono">
                                </div>
  														</div>
  													</div>
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  Celular <span class="symbol required"></span>
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_cellphone" name="cellphone" placeholder="Número de celular">
                                </div>
  														</div>
  													</div>
  												</div>
													<div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Departamento <span class="symbol required"></span>
    												  </label>
                            </div>
    												<div class="col-sm-12">
															<select name="state" id="id_state" class="form-control" >
                                <option value="empty_state">DD</option>
                                <option value="01">1</option>
                              </select>
      											</div>
                          </div>
												</div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Segundo nombre <span class="symbol required"></span>
    												  </label>
                            </div>
    												<div class="col-sm-12">
                              <input type="text" class="form-control" id="id_second_name" name="second_name" placeholder="Segundo nombre">
      											</div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Segundo apellido <span class="symbol required"></span>
    												  </label>
                            </div>
    												<div class="col-sm-12">
                              <input type="text" class="form-control" id="id_second_last_name" name="second_last_name" placeholder="Segundo apellido">
      											</div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Apellido de casada
    												  </label>
                            </div>
    												<div class="col-sm-12">
                              <input type="text" class="form-control" id="id_marry_last_name" name="marry_last_name" placeholder="Apellido de casada">
      											</div>
                          </div>
                          <div class="row">
														<div class="col-md-6">
															<div class="form-group">
														  	<div class="col-md-12">
														  		<label class="control-label">
														  			NIT <span class="symbol required"></span>
														  		</label>
														    </div>
														  	<div class="col-md-12">
														 			<input type="text" class="form-control" id="id_nit" name="nit" placeholder="Número de NIT">
														    </div>
														  </div>
														</div>
														<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  NUP
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_nup" name="nup" placeholder="Número de NUP">
                                </div>
  														</div>
  													</div>
  												</div>
													<div class="row">
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  Estatura
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_height" name="height" placeholder="Estatura en metros">
                                </div>
  														</div>
  													</div>
  													<div class="col-md-6">
  														<div class="form-group">
											          <div class="col-md-12">
  															  <label class="control-label">
  																  Peso
  															  </label>
                                </div>
                                <div class="col-md-12">
  															  <input type="text" class="form-control" id="id_weight" name="weight" placeholder="Peso en libras">
                                </div>
  														</div>
  													</div>
  												</div>
													<div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Email <span class="symbol required"></span>
    												  </label>
                            </div>
    												<div class="col-sm-12">
                              <input type="text" class="form-control" id="id_email" name="email" placeholder="Correo electrónico">
      											</div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
    													  Municipio <span class="symbol required"></span>
    												  </label>
                            </div>
    												<div class="col-sm-12">
                              <select name="city" id="id_city" class="form-control" >
                                <option value="empty_city">DD</option>
                                <option value="01">1</option>
                              </select>
      											</div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
                                Dirección <span class="symbol required"></span>
                              </label>
                            </div>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="id_address" name="address" placeholder="Dirección de residencia">
                            </div>
                          </div>
                        </div>
												<div class="col-sm-6">
													<div class="form-group">
														<div class="col-sm-12">
															<label class="control-label">
																Fecha de nacimiento <span class="symbol required"></span>
													  	</label>
														</div>
														<div class="col-sm-12">
															<div class="row">
																<div class="col-sm-4">
																	<select class="form-control" id="id_birth_day" name="birth_day">
																		<option value="empty_birth_day">Seleccionar...</option>
																		<option value="01">1</option>
																		<option value="02">2</option>
																		<option value="03">3</option>
																		<option value="04">4</option>
																		<option value="05">5</option>
																		<option value="06">6</option>
																		<option value="07">7</option>
																		<option value="08">8</option>
																		<option value="09">9</option>
																		<option value="10">10</option>
																		<option value="11">11</option>
																		<option value="12">12</option>
																		<option value="13">13</option>
																		<option value="14">14</option>
																		<option value="15">15</option>
																		<option value="16">16</option>
																		<option value="17">17</option>
																		<option value="18">18</option>
																		<option value="19">19</option>
																		<option value="20">20</option>
																		<option value="21">21</option>
																		<option value="22">22</option>
																		<option value="23">23</option>
																		<option value="24">24</option>
																		<option value="25">25</option>
																		<option value="26">26</option>
																		<option value="27">27</option>
																		<option value="28">28</option>
																		<option value="29">29</option>
																		<option value="30">30</option>
																		<option value="31">31</option>
																	</select>
																</div>
																<div class="col-sm-4">
																	<select class="form-control" id="id_birth_month" name="birth_day">
																		<option value="empty_birth_month">Seleccionar...</option>
																		<option value="01">Enero</option>
																		<option value="02">Febrero</option>
																		<option value=" 03">Marzo</option>
																		<option value="04">Abril</option>
																		<option value="05">Mayo</option>
																		<option value="06">Junio</option>
																		<option value="07">Julio</option>
																		<option value="08">Agosto</option>
																		<option value="09">Septiembre</option>
																		<option value="10">Octubre</option>
																		<option value="11">Noviembre</option>
																		<option value="12">Diciembre</option>
																	</select>
																</div>
																<div class="col-sm-4">
																	<input type="text" class="form-control" name="birth_year" id="id_birth_year" placeholder="AAAA">
																</div>
															</div>
														</div>
                          </div>
												</div>
												<div class="col-sm-6">
													<div class="row">
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  Sexo <span class="symbol required"></span>
  															  </label>
                                </div>
                                <div class="col-md-12">
																	<label class="radio-inline">
	                                  <input type="radio" class="square-teal" value="" name="radio_gender" id="id_female_gender">
	                                  Femenino
	                                </label>
	                                <label class="radio-inline">
	                                  <input type="radio" class="square-teal" value="" name="radio_gender"  id="id_male_gender">
	                                  Masculino
	                                </label>
                                </div>
  														</div>
  													</div>
  													<div class="col-md-6">
  														<div class="form-group">
                                <div class="col-md-12">
  															  <label class="control-label">
  																  Estado civil <span class="symbol required"></span>
  															  </label>
                                </div>
                                <div class="col-md-12">
																	<select name="civil_state" id="id_civil_state" class="form-control" >
		                                <option value="empty_civil_state">Seleccionar...</option>
		                                <option value="civil_state_single">Soltero</option>
																		<option value="civil_state_married">Casado</option>
																		<option value="civil_state_divorced">Divorciado</option>
																		<option value="civil_state_widower">Viudo</option>
		                              </select>
                                </div>
  														</div>
  													</div>
  												</div>
												</div>
                        <div class="col-sm-12">
                          <div class="form-group">
  												  <div class="col-sm-2 col-sm-offset-10">
  													  <button class="btn btn-blue next-step btn-block">
  														  Siguiente <i class="fa fa-arrow-circle-right"></i>
  													  </button>
  												  </div>
  											  </div>
                        </div>
                      </div>
                      <div id="step-2">
												<h2 class="StepTitle">Estudios realizados</h2>
												<div class="col-sm-12">
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
                                Titulo de estudio <span class="symbol required"></span>
                              </label>
                            </div>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="id_study_title" name="estudy_title" placeholder="Titulo de estudio">
                            </div>
                          </div>
                        </div>
												<div class="col-sm-12">
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="control-label">
                                Institución de estudio <span class="symbol required"></span>
                              </label>
                            </div>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" id="id_study_institution" name="estudy_institution" placeholder="Institución de estudio">
                            </div>
                          </div>
                        </div>
												<div class="col-sm-6">
													<div class="form-group">
														<div class="col-sm-12">
															<label class="control-label">
																Fecha de inicio de estudio <span class="symbol required"></span>
													  	</label>
														</div>
														<div class="col-sm-12">
															<div class="row">
																<div class="col-sm-4">
																	<select class="form-control" id="id_study_start_day" name="study_start_day">
																		<option value="empty_study_start_day">Seleccionar...</option>
																		<option value="01">1</option>
																		<option value="02">2</option>
																		<option value="03">3</option>
																		<option value="04">4</option>
																		<option value="05">5</option>
																		<option value="06">6</option>
																		<option value="07">7</option>
																		<option value="08">8</option>
																		<option value="09">9</option>
																		<option value="10">10</option>
																		<option value="11">11</option>
																		<option value="12">12</option>
																		<option value="13">13</option>
																		<option value="14">14</option>
																		<option value="15">15</option>
																		<option value="16">16</option>
																		<option value="17">17</option>
																		<option value="18">18</option>
																		<option value="19">19</option>
																		<option value="20">20</option>
																		<option value="21">21</option>
																		<option value="22">22</option>
																		<option value="23">23</option>
																		<option value="24">24</option>
																		<option value="25">25</option>
																		<option value="26">26</option>
																		<option value="27">27</option>
																		<option value="28">28</option>
																		<option value="29">29</option>
																		<option value="30">30</option>
																		<option value="31">31</option>
																	</select>
																</div>
																<div class="col-sm-4">
																	<select class="form-control" id="id_study_start_month" name="study_start_month">
																		<option value="empty_study_start_month">Seleccionar...</option>
																		<option value="01">Enero</option>
																		<option value="02">Febrero</option>
																		<option value=" 03">Marzo</option>
																		<option value="04">Abril</option>
																		<option value="05">Mayo</option>
																		<option value="06">Junio</option>
																		<option value="07">Julio</option>
																		<option value="08">Agosto</option>
																		<option value="09">Septiembre</option>
																		<option value="10">Octubre</option>
																		<option value="11">Noviembre</option>
																		<option value="12">Diciembre</option>
																	</select>
																</div>
																<div class="col-sm-4">
																	<input type="text" class="form-control" name="study_start_year" id="id_study_start_year" placeholder="AAAA">
																</div>
															</div>
														</div>
                          </div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<div class="col-sm-12">
															<label class="control-label">
																Fecha de finalización de estudio
													  	</label>
														</div>
														<div class="col-sm-12">
															<div class="row">
																<div class="col-sm-4">
																	<select class="form-control" id="id_study_end_day" name="study_end_day">
																		<option value="empty_study_end_day">Seleccionar...</option>
																		<option value="01">1</option>
																		<option value="02">2</option>
																		<option value="03">3</option>
																		<option value="04">4</option>
																		<option value="05">5</option>
																		<option value="06">6</option>
																		<option value="07">7</option>
																		<option value="08">8</option>
																		<option value="09">9</option>
																		<option value="10">10</option>
																		<option value="11">11</option>
																		<option value="12">12</option>
																		<option value="13">13</option>
																		<option value="14">14</option>
																		<option value="15">15</option>
																		<option value="16">16</option>
																		<option value="17">17</option>
																		<option value="18">18</option>
																		<option value="19">19</option>
																		<option value="20">20</option>
																		<option value="21">21</option>
																		<option value="22">22</option>
																		<option value="23">23</option>
																		<option value="24">24</option>
																		<option value="25">25</option>
																		<option value="26">26</option>
																		<option value="27">27</option>
																		<option value="28">28</option>
																		<option value="29">29</option>
																		<option value="30">30</option>
																		<option value="31">31</option>
																	</select>
																</div>
																<div class="col-sm-4">
																	<select class="form-control" id="id_study_end_month" name="study_end_month">
																		<option value="empty_study_end_month">Seleccionar...</option>
																		<option value="01">Enero</option>
																		<option value="02">Febrero</option>
																		<option value=" 03">Marzo</option>
																		<option value="04">Abril</option>
																		<option value="05">Mayo</option>
																		<option value="06">Junio</option>
																		<option value="07">Julio</option>
																		<option value="08">Agosto</option>
																		<option value="09">Septiembre</option>
																		<option value="10">Octubre</option>
																		<option value="11">Noviembre</option>
																		<option value="12">Diciembre</option>
																	</select>
																</div>
																<div class="col-sm-4">
																	<input type="text" class="form-control" name="study_end_year" id="id_study_end_year" placeholder="AAAA">
																</div>
															</div>
														</div>
                          </div>
												</div>

												<h2 class="StepTitle">Experiencia laboral</h2>
                        <div class="col-sm-4 col-sm-offset-8">
													<div class="col-sm-6">
														<button class="btn btn-light-grey back-step btn-block">
															<i class="fa fa-circle-arrow-left"></i> Atrás
														</button>
													</div>
													<div class="col-sm-6">
														<button class="btn btn-blue next-step btn-block">
															Siguiente <i class="fa fa-arrow-circle-right"></i>
														</button>
													</div>
												</div>
                      </div>


                      <div id="step-3">
												<h2 class="StepTitle">Grupo familiar</h2>
												<div class="form-group">
													<label class="col-sm-3 control-label">
														Card Holder Name <span class="symbol required"></span>
													</label>
													<div class="col-sm-7">
														<input type="text" class="form-control" id="card_name" name="card_name" placeholder="Text Field">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-2 col-sm-offset-3">
														<button class="btn btn-light-grey back-step btn-block">
															<i class="fa fa-circle-arrow-left"></i> Atrás
														</button>
													</div>
													<div class="col-sm-2 col-sm-offset-3">
														<button class="btn btn-blue next-step btn-block">
															Siguiente <i class="fa fa-arrow-circle-right"></i>
														</button>
													</div>
												</div>
											</div>
											<div id="step-4">
												<h2 class="StepTitle">Confirmar</h2>
												<h3>Datos personales</h3>
												<div class="form-group">
													<label class="col-sm-3 control-label">
														Username:
													</label>
													<div class="col-sm-7">
														<p class="form-control-static display-value" data-display="username"></p>
													</div>
												</div>
												<h3>Estudios realizados y Experiencia laboral</h3>
												<div class="form-group">
													<label class="col-sm-3 control-label">
														Fullname:
													</label>
													<div class="col-sm-7">
														<p class="form-control-static display-value" data-display="full_name"></p>
													</div>
												</div>
												<h3>Grupo familiar y referencias</h3>
												<div class="form-group">
													<label class="col-sm-3 control-label">
														Card Name:
													</label>
													<div class="col-sm-7">
														<p class="form-control-static display-value" data-display="card_name"></p>
													</div>
												</div>
												<div class="form-group">
                          <div class="col-sm-2 col-sm-offset-8">
														<button class="btn btn-success finish-step btn-block">
															Agregar empleado <i class="fa fa-arrow-circle-right"></i>
														</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
							<!-- end: FORM WIZARD PANEL -->
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
