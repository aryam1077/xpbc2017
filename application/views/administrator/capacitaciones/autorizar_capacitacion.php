<!-- start: MAIN CONTAINER -->
<div class="main-container">
  <!-- start: PAGE -->
  <div class="main-content">
    <!-- start: PANEL DE DETALLE -->
    <div id="panel-config" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">Detalle de la capacitación</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <dl class="dl-horizontal">
              <dt>
                Código
              </dt>
              <dd>
                1234567890
              </dd>
              <br/>
              <dt>
                Código del curso
              </dt>
              <dd>
                1234567890
              </dd>
              <br/>
              <dt>
                Tema
              </dt>
              <dd>
                Aquí va el tema de la capacitación
              </dd>
              <br/>
              <dt>
                Descripción
              </dt>
              <dd>
                Aquí va la descripción de la capacitación, este punto es muy muy largo, así que no deberia de dar problema en que los demas elementos dentro de este panel se desalinien, ojala! :v
              </dd>
              <br/>
              <dt>
                Fecha de inicio
              </dt>
              <dd>
                01/01/2017
              </dd>
              <br/>
              <dt>
                Fecha de fin
              </dt>
              <dd>
                01/01/2017
              </dd>
              <br/>
              <dt>
                Hora de inicio
              </dt>
              <dd>
                01/01/2017
              </dd>
              <br/>
              <dt>
                Hora de fin
              </dt>
              <dd>
                01/01/2017
              </dd>
              <br/>
              <dt>
                Lugar
              </dt>
              <dd>
                Aquí va el lugar donde será la capacitación
              </dd>
              <br/>
              <dt>
                # Asistentes
              </dt>
              <dd>
                99
              </dd>
              <br/>
              <dt>
                Tipo de asistencia
              </dt>
              <dd>
                Obligatoria u opcional
              </dd>
              <br/>
              <dt>
                # De reuniones
              </dt>
              <dd>
                99
              </dd>
              <br/>
              <dt>
                Costo
              </dt>
              <dd>
                $9999.00
              </dd>
              <br/>
              <dt>
                Usuario creador
              </dt>
              <dd>
                Aquí va el nombre del usuario creador
              </dd>
            </dl>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary">
          Cerrar
        </button>
      </div>
    </div>
    <!-- /.modal -->
    <!-- end: PANEL DE DETALLE-->
    <!-- start: PANEL DE MODIFICACION -->
    <div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title">Modificación De Datos</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<h4>Datos a modificar</h4>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
					</div>
					<div class="col-md-6">
						<h4>Some More Input</h4>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
						<p>
							<input class="form-control" type="text">
						</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-light-grey">
					Cerrar
				</button>
				<button type="button" class="btn btn-blue">
					Guardar Cambios
				</button>
			</div>
		</div>
<!-- end: PANEL DE MODIFICACION -->

<!-- start: PANEL DE ELIMINACION -->
<div id="panel-eliminar" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
    </button>
    <h4 class="modal-title">Eliminar registro</h4>
  </div>
  <div class="modal-body">
    <p>
      ¿Está seguro que desea eliminar este registro?
    </p>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-primary">
      Cerrar
    </button>
  </div>
</div>
<!-- /.modal -->
<!-- end: PANEL DE ELIMINACION-->

    <div class="container">
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12">
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <ol class="breadcrumb">
            <li>
              <i class="clip-grid-6"></i>
              <a href="#">
                Capacitaciones
              </a>
            </li>
            <li class="active">
              Autorizar capacitación
            </li>
            <li class="search-box">
              <form class="sidebar-search">
                <div class="form-group">
                  <input type="text" placeholder="Start Searching...">
                  <button class="submit">
                    <i class="clip-search-3"></i>
                  </button>
                </div>
              </form>
            </li>
          </ol>
          <div class="page-header">
            <h1>Autorizar Capacitación <small>Listado de capacitaciones para autorizar</small></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
      </div>
      <!-- end: PAGE HEADER -->
      <!-- start: PAGE CONTENT -->
      <div class="row">
        <div class="col-md-12">
          <!-- start: DYNAMIC TABLE PANEL -->
          <div class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-external-link-square"></i>
              Autorizar Capacitación
              <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                </a>
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                  <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                  <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                  <i class="fa fa-resize-full"></i>
                </a>
                <a class="btn btn-xs btn-link panel-close" href="#">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
            <div class="panel-body">
              <table class="sample_1 table table-striped table-bordered table-hover table-full-width" id="sample_1">
                <thead>
                  <tr>
                    <th>Código</th>
                    <th class="hidden-xs">Capacitación</th>
                    <th>Fecha Inicio</th>
                    <th class="hidden-xs">Asistentes</th>
                    <th>Operación</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Konqueror</td>
                    <td class="hidden-xs">KDE</td>
                    <td>Free</td>
                    <td class="hidden-xs">GPL</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Consultar Detalle"><i class="clip-search-2"></i></a>
                        <a href="#responsive" data-toggle="modal" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Modificar Registro"><i class="fa fa-edit"></i></a>
                        <a href="#panel-eliminar" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="clip-search-2"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times fa fa-white"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Links</td>
                    <td class="hidden-xs">Patocka, <i>et al.</i></td>
                    <td>Free</td>
                    <td class="hidden-xs">GPL</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Consultar Detalle"><i class="clip-search-2"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Modificar Registro"><i class="fa fa-edit"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="clip-search-2"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times fa fa-white"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Lunascape</td>
                    <td class="hidden-xs">Lunascape Corporation</td>
                    <td>Free</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Consultar Detalle"><i class="clip-search-2"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Modificar Registro"><i class="fa fa-edit"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="clip-search-2"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times fa fa-white"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Lynx</td>
                    <td class="hidden-xs">Montulli, Grobe, Rezac, <i>et al.</i></td>
                    <td>Free</td>
                    <td class="hidden-xs">GPL</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Consultar Detalle"><i class="clip-search-2"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Modificar Registro"><i class="fa fa-edit"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="clip-search-2"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times fa fa-white"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Maxthon</td>
                    <td class="hidden-xs">Maxthon International Limited</td>
                    <td>Free</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Consultar Detalle"><i class="clip-search-2"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Modificar Registro"><i class="fa fa-edit"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="clip-search-2"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times fa fa-white"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Midori</td>
                    <td class="hidden-xs">Christian Dywan, et al.</td>
                    <td>Free</td>
                    <td class="hidden-xs">LGPL</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Consultar Detalle"><i class="clip-search-2"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Modificar Registro"><i class="fa fa-edit"></i></a>
                        <a href="#panel-config" data-toggle="modal" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="clip-search-2"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times fa fa-white"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Mosaic</td>
                    <td class="hidden-xs">Marc Andreessen and
                    Eric Bina,
                    NCSA</td>
                    <td>non-commercial use</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Mozilla Application Suite</td>
                    <td class="hidden-xs">Mozilla Foundation</td>
                    <td>Free</td>
                    <td class="hidden-xs">tri-license</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Mozilla Firefox</td>
                    <td class="hidden-xs">Mozilla Foundation</td>
                    <td>Free</td>
                    <td class="hidden-xs">MPL</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Netscape (v.6-7) </td>
                    <td class="hidden-xs">Netscape Communications Corporation,
                    AOL</td>
                    <td>Free</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Netscape Browser (v.8)[note 2]</td>
                    <td class="hidden-xs">Mercurial Communications for
                    AOL</td>
                    <td>Free</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Netscape Communicator (v.4)[note 2]</td>
                    <td class="hidden-xs">Netscape Communications</td>
                    <td>Free</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Netscape Navigator (v.1-4)[note 2]</td>
                    <td class="hidden-xs">Netscape Communications</td>
                    <td>Free</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>Netscape Navigator 9[note 2]</td>
                    <td class="hidden-xs">Netscape Communications
                    <br>
                    (division of AOL)</td>
                    <td>Free</td>
                    <td class="hidden-xs">Proprietary</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                  <tr>
                    <td>NetSurf</td>
                    <td class="hidden-xs">The NetSurf Developers</td>
                    <td>Free</td>
                    <td class="hidden-xs">GPL</td>
                    <td class="center">
                      <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                        <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                      </div>
                      <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group">
                          <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                          </a>
                          <ul role="menu" class="dropdown-menu pull-right">
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-share"></i> Share
                              </a>
                            </li>
                            <li role="presentation">
                              <a role="menuitem" tabindex="-1" href="#">
                                <i class="fa fa-times"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                  </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- end: DYNAMIC TABLE PANEL -->
        </div>
      </div>
      <!-- end: PAGE CONTENT-->
    </div>
  </div>
  <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
