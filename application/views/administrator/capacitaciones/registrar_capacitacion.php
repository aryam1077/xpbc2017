<!-- start: MAIN CONTAINER -->
<div class="main-container">
  <!-- start: PAGE -->
  <div class="main-content">
    <!-- start: PANEL CONFIGURATION MODAL FORM -->
    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              &times;
            </button>
            <h4 class="modal-title">Panel De Configuración</h4>
          </div>
          <div class="modal-body">
            Aquí va los ajustes del panel
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              Cerrar
            </button>
            <button type="button" class="btn btn-primary">
              Guardar Cambios
            </button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12">
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <ol class="breadcrumb">
            <li>
              <i class="clip-pencil"></i>
              <a href="#">
                Capacitaciones
              </a>
            </li>
            <li class="active">
              Registrar capacitación
            </li>
            <li class="search-box">
              <form class="sidebar-search">
                <div class="form-group">
                  <input type="text" placeholder="Start Searching...">
                  <button class="submit">
                    <i class="clip-search-3"></i>
                  </button>
                </div>
              </form>
            </li>
          </ol>
          <div class="page-header">
            <h1>Registrar Capacitación <small>Asistente para registrar capacitación</small></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
      </div>
      <!-- end: PAGE HEADER -->
      <!-- start: PAGE CONTENT -->
      <div class="row">
        <div class="col-sm-12">
          <!-- start: FORM WIZARD PANEL -->
          <div class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-external-link-square"></i>
              Registrar Capacitación
              <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                  <i class="fa fa-wrench"></i>
                </a>
                <a class="btn btn-xs btn-link panel-refresh" href="#">
                  <i class="fa fa-refresh"></i>
                </a>
                <a class="btn btn-xs btn-link panel-expand" href="#">
                  <i class="fa fa-resize-full"></i>
                </a>
              </div>
            </div>
            <div class="panel-body">
              <form action="#" role="form" class="smart-wizard form-horizontal" id="form">
                <div id="wizard" class="swMain">
                  <ul>
                    <li>
                      <a href="#step-1">
                        <div class="stepNumber">
                          1
                        </div>
                        <span class="stepDesc"> Paso 1
                          <br />
                          <small>Datos Generales</small> </span>
                      </a>
                    </li>
                    <li>
                      <a href="#step-2">
                        <div class="stepNumber">
                          2
                        </div>
                        <span class="stepDesc"> Paso 2
                          <br />
                          <small>Datos de institución/persona capacitora</small> </span>
                      </a>
                    </li>
                    <li>
                      <a href="#step-3">
                        <div class="stepNumber">
                          3
                        </div>
                        <span class="stepDesc"> Paso 3
                          <br />
                          <small>Enlistar empleados</small> </span>
                      </a>
                    </li>
                    <li>
                      <a href="#step-4">
                        <div class="stepNumber">
                          4
                        </div>
                        <span class="stepDesc"> Paso 4
                          <br />
                          <small>Validación y confirmación</small> </span>
                      </a>
                    </li>
                  </ul>
                  <div class="progress progress-striped active progress-sm">
                    <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
                      <span class="sr-only"> 0% Completado </span>
                    </div>
                  </div>
                  <div id="step-1">
                    <h2 class="StepTitle">Paso 1 Datos Generales</h2>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Tema <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="tema" name="tema" placeholder="Text Field">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Descripción <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="desc" name="desc" placeholder="Text Field">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Lugar <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="lugar" name="lugar" placeholder="Text Field">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Fecha inicio <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control date-picker">
                          <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        </div>
                      </div>

                      <label class="col-sm-1 control-label">
                        Fecha fin
                      </label>
                      <div class="col-sm-3">
                        <div class="input-group">
                          <input type="text" data-date-format="dd-mm-yyyy" data-date-viewmode="years" class="form-control date-picker">
                          <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Hora inicio <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-3">
                        <div class="input-group input-append bootstrap-timepicker">
                          <input type="text" class="form-control time-picker">
                          <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                        </div>
                      </div>
                      <label class="col-sm-1 control-label">
                        Hora fin
                      </label>
                      <div class="col-sm-3">
                        <div class="input-group input-append bootstrap-timepicker">
                          <input type="text" class="form-control time-picker">
                          <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-7 control-label">
                        Costo <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="tema" name="tema" placeholder="Text Field">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-7 control-label">
                        Asistencia <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-5">
                        <div class="col-sm-4">
                          <label class="radio-inline">
                            <input type="radio" class="square-teal" value="" checked="" name="optionsRadios13">
                            Obligatoria
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <label class="radio-inline">
                            <input type="radio" class="square-teal" value="" checked="" name="optionsRadios13">
                            Opcional
                          </label>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-7 control-label">
                       Número de asistentes <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="tema" name="tema" placeholder="Text Field">
                      </div>
                    </div>


                    <div class="form-group">
                      <div class="col-sm-2 col-sm-offset-8">
                        <button class="btn btn-blue next-step btn-block">
                          Siguiente <i class="fa fa-arrow-circle-right"></i>
                        </button>
                      </div>
                    </div>
                  </div>

                  <div id="step-2">
                    <h2 class="StepTitle">Paso 2 Datos de institución/persona capacitora</h2>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Institución Capacitadora <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <select class="form-control" id="institucion" name="institucion">
                          <option value="">&nbsp;</option>
                          <option value="Country 1">Institución 1</option>
                          <option value="Country 2">Institución 2</option>
                          <option value="Country 3">Institución 3</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Persona Capacitadora <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <select class="form-control" id="persona" name="persona">
                          <option value="">&nbsp;</option>
                          <option value="Country 1">Persona 1</option>
                          <option value="Country 2">Persona 2</option>
                          <option value="Country 3">Persona 3</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Dirección <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Text Field">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        NIT <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="nit" name="nit" placeholder="Text Field">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Teléfono <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Text Field">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Correo <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" id="correo" name="correo" placeholder="Text Field">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-light-grey back-step btn-block">
                          <i class="fa fa-circle-arrow-left"></i> Atras
                        </button>
                      </div>
                      <div class="col-sm-2 col-sm-offset-3">
                        <button class="btn btn-blue next-step btn-block">
                          Siguiente <i class="fa fa-arrow-circle-right"></i>
                        </button>
                      </div>
                    </div>
                  </div>

                  <div id="step-3">
                    <h2 class="StepTitle">Paso 3 Empleados a capacitar</h2>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Unidad/Dirección <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <select class="form-control" id="uni_dir" name="uni_dir">
                          <option value="">&nbsp;</option>
                          <option value="Country 1">Unidad 1</option>
                          <option value="Country 2">Unidad 2</option>
                          <option value="Country 3">Direción 1</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">
                        Nombre Empleado <span class="symbol required"></span>
                      </label>
                      <div class="col-sm-7">
                        <select class="form-control" id="nombre_empleado" name="nombre_empleado">
                          <option value="">&nbsp;</option>
                          <option value="Country 1">Empleado 1</option>
                          <option value="Country 2">Empleado 2</option>
                          <option value="Country 3">Empleado 3</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-2 col-sm-offset-8">
                        <button class="btn btn-blue next-step btn-block">
                          Enlistar <i class="fa fa-arrow-circle-down"></i>
                        </button>
                      </div>
                    </div>

                    <hr>

                    <div class="row">
                      <div class="col-md-12">
                        <!-- start: DYNAMIC TABLE PANEL -->
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i>
                            Dynamic Table
                            <div class="panel-tools">
                              <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                              </a>
                              <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                <i class="fa fa-wrench"></i>
                              </a>
                              <a class="btn btn-xs btn-link panel-refresh" href="#">
                                <i class="fa fa-refresh"></i>
                              </a>
                              <a class="btn btn-xs btn-link panel-expand" href="#">
                                <i class="fa fa-resize-full"></i>
                              </a>
                            </div>
                          </div>
                          <div class="panel-body">
                            <table class="table sample_1 table-striped table-bordered table-hover table-full-width" id="sample_1">
                              <thead>
                                <tr>
                                  <th>Browser</th>
                                  <th class="hidden-xs">Creator</th>
                                  <th>Cost (
                                  USD)</th>
                                  <th class="hidden-xs"> Software license</th>
                                  <th>Current
                                  layout engine</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Amaya</td>
                                  <td class="hidden-xs">W3C,
                                  INRIA</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">W3C</td>
                                  <td>Amaya</td>
                                </tr>
                                <tr>
                                  <td>AOL Explorer</td>
                                  <td class="hidden-xs">America Online, Inc</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Trident</td>
                                </tr>
                                <tr>
                                  <td>Arora</td>
                                  <td class="hidden-xs">Benjamin C. Meyer</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Avant</td>
                                  <td class="hidden-xs">Avant Force</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Tri-engine</td>
                                </tr>
                                <tr>
                                  <td>Camino</td>
                                  <td class="hidden-xs">The Camino Project</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">tri-license</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>Chromium</td>
                                  <td class="hidden-xs">Google</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">BSD</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Dillo</td>
                                  <td class="hidden-xs">The Dillo team</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>Dillo</td>
                                </tr>
                                <tr>
                                  <td>Dooble</td>
                                  <td class="hidden-xs">Dooble Team</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>ELinks</td>
                                  <td class="hidden-xs">Baudis, Fonseca, <i>et al.</i></td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>built-in</td>
                                </tr>
                                <tr>
                                  <td>Web</td>
                                  <td class="hidden-xs">Marco Pesenti Gritti</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Flock</td>
                                  <td class="hidden-xs">Flock Inc</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Galeon</td>
                                  <td class="hidden-xs">Marco Pesenti Gritti</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>Google Chrome</td>
                                  <td class="hidden-xs">Google</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Google Chrome Terms of Service</td>
                                  <td>Blink</td>
                                </tr>
                                <tr>
                                  <td>GNU IceCat</td>
                                  <td class="hidden-xs">GNU</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">MPL</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>iCab</td>
                                  <td class="hidden-xs">Alexander Clauss</td>
                                  <td>$20 (Pro)</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Internet Explorer</td>
                                  <td class="hidden-xs">Microsoft,
                                  <br>
                                  Spyglass</td>
                                  <td>comes with Windows</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Trident</td>
                                </tr>
                                <tr>
                                  <td>Internet Explorer for Mac (terminated)</td>
                                  <td class="hidden-xs">Microsoft</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Tasman</td>
                                </tr>
                                <tr>
                                  <td>K-Meleon</td>
                                  <td class="hidden-xs">Dorian, KKO, <i>et al.</i></td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>Konqueror</td>
                                  <td class="hidden-xs">KDE</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>KHTML</td>
                                </tr>
                                <tr>
                                  <td>Links</td>
                                  <td class="hidden-xs">Patocka, <i>et al.</i></td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>built-in</td>
                                </tr>
                                <tr>
                                  <td>Lunascape</td>
                                  <td class="hidden-xs">Lunascape Corporation</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Tri-engine</td>
                                </tr>
                                <tr>
                                  <td>Lynx</td>
                                  <td class="hidden-xs">Montulli, Grobe, Rezac, <i>et al.</i></td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>built-in</td>
                                </tr>
                                <tr>
                                  <td>Maxthon</td>
                                  <td class="hidden-xs">Maxthon International Limited</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Trident</td>
                                </tr>
                                <tr>
                                  <td>Midori</td>
                                  <td class="hidden-xs">Christian Dywan, et al.</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">LGPL</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Mosaic</td>
                                  <td class="hidden-xs">Marc Andreessen and
                                  Eric Bina,
                                  NCSA</td>
                                  <td>non-commercial use</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>built-in</td>
                                </tr>
                                <tr>
                                  <td>Mozilla Application Suite</td>
                                  <td class="hidden-xs">Mozilla Foundation</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">tri-license</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>Mozilla Firefox</td>
                                  <td class="hidden-xs">Mozilla Foundation</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">MPL</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>Netscape (v.6-7) </td>
                                  <td class="hidden-xs">Netscape Communications Corporation,
                                  AOL</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>Netscape Browser (v.8)[note 2]</td>
                                  <td class="hidden-xs">Mercurial Communications for
                                  AOL</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Gecko, Trident</td>
                                </tr>
                                <tr>
                                  <td>Netscape Communicator (v.4)[note 2]</td>
                                  <td class="hidden-xs">Netscape Communications</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Mosaic</td>
                                </tr>
                                <tr>
                                  <td>Netscape Navigator (v.1-4)[note 2]</td>
                                  <td class="hidden-xs">Netscape Communications</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Mosaic</td>
                                </tr>
                                <tr>
                                  <td>Netscape Navigator 9[note 2]</td>
                                  <td class="hidden-xs">Netscape Communications
                                  <br>
                                  (division of AOL)</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>NetSurf</td>
                                  <td class="hidden-xs">The NetSurf Developers</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GPL</td>
                                  <td>NetSurf built-in</td>
                                </tr>
                                <tr>
                                  <td>OmniWeb</td>
                                  <td class="hidden-xs">The Omni Group</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Opera</td>
                                  <td class="hidden-xs">Opera Software</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Presto</td>
                                </tr>
                                <tr>
                                  <td>Opera Mobile</td>
                                  <td class="hidden-xs">Opera Software</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Presto</td>
                                </tr>
                                <tr>
                                  <td>Origyn Web Browser</td>
                                  <td class="hidden-xs">Sand-labs</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">BSD</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>QupZilla</td>
                                  <td class="hidden-xs">David Rosca</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GNU GPLv3</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Safari</td>
                                  <td class="hidden-xs">Apple Inc.</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>SeaMonkey</td>
                                  <td class="hidden-xs">SeaMonkey Council</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">tri-license</td>
                                  <td>Gecko</td>
                                </tr>
                                <tr>
                                  <td>Shiira</td>
                                  <td class="hidden-xs">Happy Macintosh Developing Team</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">BSD</td>
                                  <td>WebKit</td>
                                </tr>
                                <tr>
                                  <td>Sleipnir</td>
                                  <td class="hidden-xs">Fenrir Inc.</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Trident</td>
                                </tr>
                                <tr>
                                  <td>Torch Browser</td>
                                  <td class="hidden-xs">Torch Media</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Proprietary</td>
                                  <td>Webkit</td>
                                </tr>
                                <tr>
                                  <td>Uzbl</td>
                                  <td class="hidden-xs">Dieter Plaetinck</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">GNU GPLv3</td>
                                  <td>Webkit</td>
                                </tr>
                                <tr>
                                  <td>WorldWideWeb (Later renamed Nexus)</td>
                                  <td class="hidden-xs">Tim Berners-Lee</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">Public domain</td>
                                  <td>NeXTSTEP built-in</td>
                                </tr>
                                <tr>
                                  <td>w3m</td>
                                  <td class="hidden-xs">Akinori Ito</td>
                                  <td>Free</td>
                                  <td class="hidden-xs">MIT</td>
                                  <td>-</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- end: DYNAMIC TABLE PANEL -->
                      </div>

                      <div class="form-group">
                        <div class="col-md-12">
                          <div class="col-ms-1">
                          </div>
                          <div class="col-sm-8 ">
                            <label class="checkbox-inline">
                              <input type="checkbox" class="teal" value="" checked="checked">
                              Generar PDF (Lista de asistencia)
                            </label>
                          </div>
                          <div class="col-sm-2 ">
                            <button class="btn btn-light-grey back-step btn-block">
                              <i class="fa fa-circle-arrow-left"></i> Atras
                            </button>
                          </div>
                          <div class="col-sm-2 ">
                            <button class="btn btn-blue next-step btn-block">
                              Siguiente <i class="fa fa-arrow-circle-right"></i>
                            </button>
                          </div>
                        </div>
                      </div>

                    </div>

                    </div>



                  <div id="step-4">
                    <h2 class="StepTitle">Paso 4 Validación Y Confirmación</h2>
                    <div class="form-group">
                      <div class="col-sm-2 col-sm-offset-8">
                        <button class="btn btn-success finish-step btn-block">
                          Finish <i class="fa fa-arrow-circle-right"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- end: FORM WIZARD PANEL -->
        </div>
      </div>
      <!-- end: PAGE CONTENT-->
    </div>
  </div>
  <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
