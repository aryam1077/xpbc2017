<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="<?= base_url() ?>template/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?= base_url() ?>template/assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?= base_url() ?>template/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/autosize/jquery.autosize.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/select2/select2.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/summernote/build/summernote.min.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/ckeditor/ckeditor.js"></script>
		<script src="<?= base_url() ?>template/assets/plugins/ckeditor/adapters/jquery.js"></script>
		<script src="<?= base_url() ?>template/assets/js/form-elements.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				FormElements.init();
			});
		</script>
	</body>
	<!-- end: BODY -->
</html>
