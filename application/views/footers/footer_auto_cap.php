<!-- start: FOOTER -->
<div class="footer clearfix">
  <div class="footer-inner">
    2014 &copy; clip-one by cliptheme.
  </div>
  <div class="footer-items">
    <span class="go-top"><i class="clip-chevron-up"></i></span>
  </div>
</div>
<!-- end: FOOTER -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/less/less-1.5.0.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/js/main.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA FORMULARIO) -->

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA FORMULARIO) -->

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA LA TABLA)-->
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="<?= base_url() ?>template/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA LA TABLA)-->

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA LOS MODALS)-->
<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="assets/js/ui-modals.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA LOS MODALS)-->

<script>
  jQuery(document).ready(function() {
    Main.init();
    TableData.init();
  });
</script>
</body>
<!-- end: BODY -->
</html>
