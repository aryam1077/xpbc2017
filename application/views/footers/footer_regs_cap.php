<!-- start: FOOTER -->
<div class="footer clearfix">
  <div class="footer-inner">
    2014 &copy; clip-one by cliptheme.
  </div>
  <div class="footer-items">
    <span class="go-top"><i class="clip-chevron-up"></i></span>
  </div>
</div>
<!-- end: FOOTER -->

<!--  REFERENCIAS A JS Y PLUGIS YA REVISADAS PARA QUE SOLO UNA VEZ SE LLAMEN
      LAS COMUNES SE AGRUPARON EN LOS PRIMEROS SRC Y LOS ELEMENTOS INDIVIDUALES
      SE AGRUPAN PARA MEJOR COMPRESION-->

<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/less/less-1.5.0.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/js/main.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA WIZARD)-->
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/js/form-wizard.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA WIZARD)-->

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA FORMULARIO) -->
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/autosize/jquery.autosize.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jquery-maskmoney/jquery.maskMoney.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpicker/js/commits.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/summernote/build/summernote.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/js/form-elements.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA FORMULARIO) -->

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA LA TABLA)-->
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?= base_url() ?>template/assets/js/table-data.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA LA TABLA)-->
<script>
  jQuery(document).ready(function() {
    Main.init();
    FormWizard.init();
    FormElements.init();
    TableData.init();
  });
</script>
</body>
<!-- end: BODY -->
</html>
