<!-- start: FOOTER -->
  <div class="footer clearfix">
  	<div class="footer-inner">
  		2014 &copy; clip-one by cliptheme.
  	</div>
  	<div class="footer-items">
  		<span class="go-top"><i class="clip-chevron-up"></i></span>
  	</div>
  </div>
  <!-- end: FOOTER -->
  <!-- start: MAIN JAVASCRIPTS -->
  <!--[if lt IE 9]>
  <script src="assets/plugins/respond.min.js"></script>
  <script src="assets/plugins/excanvas.min.js"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <![endif]-->
  <!--[if gte IE 9]><!-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <!--<![endif]-->
  <script src="<?= base_url() ?>template/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/blockUI/jquery.blockUI.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/iCheck/jquery.icheck.min.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/less/less-1.5.0.min.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
  <script src="<?= base_url() ?>template/assets/js/main.js"></script>
  <!-- end: MAIN JAVASCRIPTS -->
  <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
  <script src="<?= base_url() ?>template/assets/plugins/flot/jquery.flot.js"></script>
	<script src="<?= base_url() ?>template/assets/plugins/flot/jquery.flot.pie.js"></script>
	<script src="<?= base_url() ?>template/assets/plugins/flot/jquery.flot.resize.min.js"></script>

  <script src="<?= base_url() ?>template/assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
  <script src="<?= base_url() ?>template/assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>

  <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
  <script>
    jQuery(document).ready(function() {
      Main.init();
      //Index.init();
    });
  </script>
</body>
</html>
