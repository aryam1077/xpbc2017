<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.3 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>Clip-One - Responsive Admin Template</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/fonts/style.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/css/main.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/css/main-responsive.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/css/theme_light.css" id="skin_color">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/css/print.css" media="print"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/select2/select2.css">
		<!--[if IE 7]>
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->

		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA EL FORMULARIO)-->

		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/datepicker/css/datepicker.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>template/assets/plugins/summernote/build/summernote.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS PARA EL FORMULARIO)-->

		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY  (ELEMENTOS DE LA TABLA)-->

		<link rel="stylesheet" href="<?= base_url() ?>template/assets/plugins/DataTables/media/css/DT_bootstrap.css" />
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY (ELEMENTOS DE LA TABLA)-->

		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY (NO HAY PARA EL WIZARD)-->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY (NO HAY PARA EL WIZARD)-->

		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY (ESTO ES PARA LOS MODALS)-->
		<link href="<?= base_url() ?>template/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
		<link href="<?= base_url() ?>template/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY (ESTO ES PARA LOS MODALS)-->

	</head>
	<!-- end: HEAD -->
