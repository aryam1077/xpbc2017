<!-- start: BODY -->
<body class="login example2">
	<div class="main-login col-sm-4 col-sm-offset-4">
		<div class="logo">CLIP<i class="clip-clip"></i>ONE</div>
		<!-- start: LOGIN BOX -->
		<div class="box-login">
			<h3>Inicio de sesión.</h3>
			<p>
				Ingrese su usuario y contraseña para iniciar sesión.
			</p>
			<form class="form-login" action="<?= base_url() ?>login/validate_user" method="post">
				<fieldset>
					<div class="form-group">
						<span class="input-icon">
							<input type="text" class="form-control" name="username" placeholder="Usuario">
							<i class="fa fa-user"></i> </span>
					</div>
					<div class="form-group form-actions">
						<span class="input-icon">
							<input type="password" class="form-control password" name="password" placeholder="Contraseña">
							<i class="fa fa-lock"></i>
							<a class="forgot" href="#">
								Olvidé mi contraseña
							</a> </span>
					</div>
					<?php
						// Encript Token
						$token = md5(uniqid(rand(),true));
						$this->session->set_userdata('token', $token);
					?>
					<!--Send hidden values -->
					<input type="hidden" id="hiddenToken" name="hiddenToken" value="<?php echo $token ?>">
					<div class="form-actions">
						<button type="submit" class="btn btn-primary pull-right">
							Iniciar sesión
						</button>
					</div>
				</fieldset>
			</form>
		</div>
		<!-- end: LOGIN BOX -->
		<!-- start: FORGOT BOX -->
		<div class="box-forgot">
			<h3>¿Olvido su contraseña?</h3>
			<p>
				Puede notificar al administrador del sistama que ha olvidado su contraseña
				completando los datos solicitados por el formulario.
			</p>
			<form class="form-forgot">
				<fieldset>
					<div class="form-actions">
						<a class="btn btn-light-grey go-back">
							Atrás
						</a>
						<button type="submit" class="btn btn-primary pull-right">
							Enviar email
						</button>
					</div>
				</fieldset>
			</form>
		</div>
		<!-- end: FORGOT BOX -->
		<!-- start: COPYRIGHT -->
		<div class="copyright">
			2017 &copy; XPBC
		</div>
		<!-- end: COPYRIGHT -->
		<br><br>
		<?php
			if($this->session->flashdata('invalid_user') == 'Credenciales inválidas') {
		?>
		<div class="col-md-12">
			<div class="alert alert-danger">
				<button data-dismiss="alert" class="close">
				&times;
				</button>
				<i class="fa fa-times-circle"></i>
				<strong>Error!</strong>
				<?php
					echo $this->session->flashdata('invalid_user');
					// Clean flashdata
					$this->session->set_flashdata('invalid_user','');
				?>
			</div>
		</div>
		<?php
			}
		?>
	</div>
